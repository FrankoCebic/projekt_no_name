
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE `knjige` (
  `id` int(5) NOT NULL,
  `naziv` varchar(40) NOT NULL,
  `autor` varchar(40) NOT NULL,
  `stanje` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `knjige` (`id`, `naziv`, `autor`, `stanje`) VALUES
(11, 'da', 'da', 7),
(12, 'Judita', 'Marko Marulic', 8),
(13, 'Davidijada', 'Marko Marulic', 8);



ALTER TABLE `knjige`
  ADD UNIQUE KEY `id` (`id`);

ALTER TABLE `knjige`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
