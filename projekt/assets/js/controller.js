//controller.js
//varijable cd ili cds - u funkciji camelcase Cd ili Cds


(function(){
	var list = $('#cdsList');
	var baseUrl = '/projekt_no_name/projekt/api/cds/';
	//readCds();
	$("#saveButton").click(submit);
	$("#updateButton").click(update);
	$("#deleteButton").click(deleteCd);
	$("#readButton").click();

	function submit() {
		var inputData = $('#inputForm').serialize();
		var id = $( "input[name$='id']" ).val();
		if (id) {
			updateCd(inputData);
		}else {
			createCd(inputData);
		}
	}
	//--------------------------------------------------------------------------

	 function update() {
		var inputData = $('#updateForm').serialize();
		var id = $( "input[name$='id']" ).val();
		if (id) {
			createCd(inputData);
		}else {		
			updateCd(inputData);
		}
	}


	function createCd(data) {
		var url = baseUrl + 'create.php';
		console.log("ne");
		httpRequest(url, 'POST', insertCdToList, data);
	}

	function deleteCd(event) {
		var data = {id: event.data.id};
		var url = baseUrl + 'delete.php';
		httpRequest(url, 'POST', removeCdFromList, data);
	}

	//--------------------------------------------------------------------------

	function readCD(event) {
		 var url = baseUrl + 'read.php';
		 var data = {id: event.data.id};
		 httpRequest(url, 'GET', syncCdToInputForm, data);
	}

	//--------------------------------------------------------------------------

	function readCds() {
		var url = baseUrl + 'read.php';
		httpRequest(url, 'GET', insertCdsToList);
	}


	//--------------------------------------------------------------------------

	function updateCd(data) {
		var url = baseUrl + 'update.php';
		console.log("da");
		httpRequest(url, 'POST', updateCdInList, data);
	}

	//--------------------------------------------------------------------------
	
	function syncCdToInputForm(cds) {
		for (var key in cds) {
			var value = cds[key];
			$( "input[name$='" + key + "']" ).val(value);
		}
	}

	//--------------------------------------------------------------------------

	function httpRequest(url, type, callback, data) {
		//console.log(">>>>SEND: ", data);
		$.ajax({
			url: url,
			type: type,
			success: function(data) {
				//console.log('>>>>RECEIVE: ', data);
				callback(JSON.parse(data));
      		},
			data:data
		});
	}

	//==========================================================================

	function appendDeleteButtonToLi(cd, li) {
		var btn = createButton('Delete ', cd);
		btn.on('click',{id:cd.id}, deleteCd);
		(li).append(btn);
	}

	//--------------------------------------------------------------------------

	function appendUpdateButtonToLi(cd, li) {
		var btn = createButton('Update ', cd);
		btn.on('click',{id:cd.id}, readCD);
		(li).append(btn);
	}

	//--------------------------------------------------------------------------

	function createButton(label, cd) {
		var str = label + cd.id;
		var btn = $('<button/>', {text:str});
		return btn;
	}

	//--------------------------------------------------------------------------

	function createCdLi(cd) {
		alert(cdToStr(cd));
		//var li = $('<li/>', {id:'cd'+cd.id});
		//var str = cdToStr(cd);
	    //var p = $('<p/>', {text:str}).appendTo(li);
		//appendDeleteButtonToLi(cd, li);
		//appendUpdateButtonToLi(cd, li);
		//return li;
	}

	function createCdLi2(cd) {
		var li = $('<li/>', {id:'cd'+cd.id});
		var str = cdToStr(cd);
	    var p = $('<p/>', {text:str}).appendTo(li);
		//appendDeleteButtonToLi(cd, li);
		//appendUpdateButtonToLi(cd, li);
		return li;
	}


	//--------------------------------------------------------------------------

	function insertCdToList(cd) {
		var li = createCdLi(cd);
		list.append(li);
	}

	//--------------------------------------------------------------------------

	function insertCdsToList(cds) {
		list.empty();
		for (i in cds) {
			var li = createCdLi2(cds[i]);
			list.append(li);
		}
	}

	//--------------------------------------------------------------------------

	function removeCdFromList(cd) {
		var cdId = '#cd' + cd.id;
		$(cdId).remove();
	}

	//--------------------------------------------------------------------------

	function cdToStr(cd) {
		return 'Naziv: ' + cd.naziv + ', Autor: ' + cd.autor  + ',  Stanje: ' + cd.stanje + ', Identifikacijski broj: ' + cd.id ;
	}
	//--------------------------------------------------------------------------

	function updateCdInList(cd) {
		clearFromFields(cd);
		var cdId = '#cd' + cd.id + ' p';
		var str = cdToStr(cd);
		$(cdId).text(str);
	}

	//--------------------------------------------------------------------------

	function clearFromFields(cds) {
		for (var key in cds) {
			$( "input[name$='" + key + "']" ).val("");
		}
	}

	//--------------------------------------------------------------------------

})();