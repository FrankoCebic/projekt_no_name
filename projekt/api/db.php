<?php
	$ERRORS = array(
   		'CONNECT_MYSQL' => 'Error connectiong to mysql ',
   		'SELECT_DB' => 'Error connectiong to db ', 
   		'QUERRY' => 'Querry error '
	);

	$HOST = 'localhost';
	$USER = 'root';
	$PASSWORD = '';
	$DATABASE = 'baza_knjiga';
	

	$link = connectToMysqlDb($HOST, $USER, $PASSWORD, $DATABASE);
	#$db = selectDb($DATABASE, $link);

	#---------------------------------------------------------------------------
	
	function connectToMysqlDb($host, $user, $password, $database) {
		$link = mysqli_connect($host, $user, $password, $database);
		if (!$link){
			die($ERRORS['CONNECT_MYSQL'] . mysqli_error());
		}
		return $link;
	}

	#---------------------------------------------------------------------------


	#---------------------------------------------------------------------------

	function selectDb($database, $link) {
		$db = mysqli_select_db($database, $link);
		if (!$db){
			die($ERRORS['SELECT_DB'] . mysqli_error());
		}
		return $db;
	}
	