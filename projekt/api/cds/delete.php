<?php
	require_once('../db.php');
	require_once('const.php');

	#---------------------------------------------------------------------------

	$response = Array('id' => null);

	if (isset($_POST['id'])) {
		$id = $_POST['id'];
		$result = deleteFromTable($link, $TABLE, $id);
		$response['id'] = $result ? $id : null;
	}

	$link->close();

	$json = json_encode($response);
	echo $json;


	#---------------------------------------------------------------------------

	function deleteFromTable($link, $table, $id) {
		$query = "DELETE FROM $table WHERE id = $id;";
		$result = $link->query($query);
		return $result;
	}

	#---------------------------------------------------------------------------
