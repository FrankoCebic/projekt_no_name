<?php
	require_once('../db.php');
	require_once('const.php');

	#---------------------------------------------------------------------------

	$json = array();
	if(isset($_GET['naziv'])) {
		$naziv = $_GET['naziv'];
		$result = selectFromTable($link, $TABLE, $id);
		$json = resultToJsonObject($result);
	} else {
		$result = selectAllFromTable($link, $TABLE);
		$json = resultToJsonArray($result);
	}

	
	echo $json;


	$result->free_result();
	$link->close();


	#---------------------------------------------------------------------------

	function selectFromTable($link, $table, $id) {
		$query = "SELECT * FROM $table WHERE naziv = $naziv;";
		$result = $link->query($query);
		return $result;
	}

	#---------------------------------------------------------------------------

	function selectAllFromTable($link, $table) {
		$query = "SELECT * FROM $table;";
		$result = $link->query($query);
		return $result;
	}

	#---------------------------------------------------------------------------

	function resultToJsonArray($result) {
		$rows = array();
		while($row = $result->fetch_assoc()){
			$rows[] = $row;
		}
		return json_encode($rows);		
	}
	
	#---------------------------------------------------------------------------	

	function resultToJsonObject($result) {
		$rows = array();
		while( $row = $result->fetch_assoc() ){
			$rows[] = $row;
		}
		return json_encode($rows[0]);
	}
	
	#---------------------------------------------------------------------------	