<?php
	
	require_once('../db.php');
	require_once('const.php');
	#---------------------------------------------------------------------------

	$data = Array();
	if (!empty($_POST)) {	
		$result = insertIntoTable($link, $TABLE, $_POST);
		if ($result) {
			$id = $link->insert_id;
			$_POST['id'] = $id;
			$json = json_encode($_POST);
			echo $json;
		}
	}

	$link->close();

	#---------------------------------------------------------------------------

	function insertIntoTable($link, $table, $data) {
   		$sql  = "INSERT INTO $table";
   		$sql .= " (`".implode("`, `", array_keys($data))."`)";
   		$sql .= " VALUES ('".implode("', '", $data)."') ";
   		$sql .= ';';
		$result = $link->query($sql);
		return $result;
	}
	

		